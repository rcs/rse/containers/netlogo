from ubuntu as builder
run apt-get update && apt-get install -y curl && rm -rf /var/lib/apt/lists/*

arg netlogo_version=6.1.1
run mkdir /netlogo
run curl https://ccl.northwestern.edu/netlogo/${netlogo_version}/NetLogo-${netlogo_version}-64.tgz | tar xz --strip-components 1 -C /netlogo

from debian
run apt-get update && apt-get install -y default-jre ca-certificates x11-apps && rm -rf /var/lib/apt/lists/*
copy --from=builder /netlogo /netlogo

ENV LC_ALL C.UTF-8
ENV PATH="/netlogo:${PATH}"
ENV JAVA_HOME=/usr
ENV PS1="$PS1<netlogo ${netlogo_version}> "

cmd ["NetLogo"]
